#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import base64
import subprocess
import configparser
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GLib
from os import path

# ===== Connection settings: ======
# ===== freerdp-x11:

string_l = "xfreerdp /drive:share,/media /bpp:15 /jpeg /cert-ignore /v:192.168.1.128"
string_r = "xfreerdp /drive:share,/media /bpp:8 /jpeg /cert-ignore /v:external.domain:3389"

# ===== rdesktop:
#string_l = "rdesktop -z -a 15 -k en-us -r disk:share=/media 192.168.1.128"
#string_r = "rdesktop -z -a 8 -k en-us -r disk:share=/media external.domain:3389"

conf_file = path.expanduser("~") + '/.config/pygtkrdesktop.conf'

class Table(Gtk.Window):

    def Connect(self, widget, data=None):
        name = login_entry.get_text()
        password = password_entry.get_text()
        SaveLogin(name, password)
        if ( data == "local" ):
            string = string_l + " /u:" + name + " /p:" + password
# ===== rdesktop:
#            string = string_l + " -u " + name + " -p " + password
        if ( data == "remote" ):
            string = string_r + " /u:" + name + " /p:" + password
# ===== rdesktop:
#            string = string_r + " -u " + name + " -p " + password

        if fullscreen_check.get_active():
            string += " /f"
# ===== rdesktop:
#            string += " -f"
        else:
            string += " /w:800 /h:600"
# ===== rdesktop:
#            string += ""

        p = subprocess.Popen(string, shell=True, stderr=subprocess.PIPE, )
        streamdata = p.communicate()[0]
        rc = p.returncode

        if ( (rc == 0) or (rc == 62)):
            Gtk.main_quit()
        else:
            if (rc == 132):
                status_entry_label.set_markup('<b><span color="red">Error: wrong login or password</span></b>');
            else:
                status_entry_label.set_markup('<b><span color="red">Error: check your connection</span></b>');

#        Uncomment to check errorcode:
#        print "Error code", rc

    def LoginEnterPressed(self, widget, event):
        if Gdk.keyval_name(event.keyval) == 'Return':
            password_entry.grab_focus()
            return True
        return False

    def PasswordEnterPressed(self, widget, event):
        if Gdk.keyval_name(event.keyval) == 'Return':
            self.Connect(widget, "local")
            return True
        return False

    def delete_event(self, widget, event, data=None):
        Gtk.main_quit()
        return False

    def __init__(self):
        super().__init__(title="pyGtkRDP")
        self.window = Gtk.Window()
        self.window.set_title("Connection...")
        self.window.connect("delete_event", self.delete_event)
        self.window.set_border_width(10)
        table = Gtk.Table(n_rows=9, n_columns=2, homogeneous=True)
        self.window.add(table)

        login_entry_label = Gtk.Label (label="Username:")
        table.attach(login_entry_label, 0, 2, 0, 1)
        login_entry_label.show()

        login_entry.set_max_length(20)
        table.attach(login_entry, 0, 2, 1, 2)
        if conf.has_option("login", "saved"):
            login_entry.set_text(conf.get("login", "saved"))
        login_entry.show()
        login_entry.connect('key-press-event', self.LoginEnterPressed)

        password_entry_label = Gtk.Label (label="Password:")
        table.attach(password_entry_label, 0, 2, 2, 3)
        password_entry_label.show()

        password_entry.set_max_length(30)
        password_entry.set_visibility(False)
        table.attach(password_entry, 0, 2, 3, 4)
        password_entry.show()
        if conf.has_option("login", "saved"):
            password_entry.grab_focus()
        if conf.has_option("login", "password"):
            password_entry.set_text(base64.b32decode(base64.b16decode(base64.b64decode
                        (conf.get("login", "password")))).decode('utf-8'))
        password_entry.connect('key-press-event', self.PasswordEnterPressed)

        table.attach(status_entry_label, 0, 2, 4, 5)
        status_entry_label.show()

        fullscreen_check.set_tooltip_text("If checked – remote connection will be fullscreen, if no – windowed. Press CTRL+ALT+ENTER to toggle fullscreen.")
        if conf.has_option("login", "fullscreen"):
            if conf.get("login", "fullscreen") == 'True':
                fullscreen_check.set_active(True)
            else:
                fullscreen_check.set_active(False)
        else:
            fullscreen_check.set_active(True)
        table.attach(fullscreen_check, 0, 2, 5, 6)
        fullscreen_check.show()

        password_check.set_tooltip_text("If checked – passwrod will be saved in file. It's unsafe feature, please, be careful.")
        if conf.has_option("login", "savepassword"):
            if conf.get("login", "savepassword") == 'True':
                password_check.set_active(True)
            else:
                password_check.set_active(False)
        else:
            password_check.set_active(False)
        table.attach(password_check, 0, 2, 6, 7)
        password_check.show()

        local_button = Gtk.Button(label="Local connection")
        local_button.set_tooltip_text("Connection via LAN (using internal IP.")
        local_button.connect("clicked", self.Connect, "local")
        table.attach(local_button, 0, 1, 7, 8)
        local_button.show()

        remote_button = Gtk.Button(label="External connection")
        remote_button.set_tooltip_text("Connection via WAN (using external IP).")
        remote_button.connect("clicked", self.Connect, "remote")
        table.attach(remote_button, 1, 2, 7, 8)
        remote_button.show()

        button = Gtk.Button(label="Exit")
        button.connect("clicked", lambda w: Gtk.main_quit())
        table.attach(button, 0, 2, 8, 9)
        button.show()

        table.show()
        self.window.show()

def SaveLogin(login, password):
    if not conf.has_section("login"):
        conf.add_section("login")
    conf.set("login", "saved", login)
    conf.set("login", "fullscreen", fullscreen_check.get_active())
    conf.set("login", "savepassword", password_check.get_active())
    if password_check.get_active():
        password = password.encode()
        password = base64.b64encode(base64.b16encode(
                                  base64.b32encode(password)))
        password = password.decode('utf-8')
        conf.set("login", "password", password)
    else:
        conf.remove_option("login", "password")
    conf.write(open(conf_file, "w"))

def main():
    Gtk.main()
    return 0

login_entry = Gtk.Entry()
password_entry = Gtk.Entry()
status_entry_label = Gtk.Label (label="CTRL+ALT+ENTER - toggle fullscreen")
fullscreen_check = Gtk.CheckButton (label="Fullscreen mode")
password_check = Gtk.CheckButton (label="Save password (unsafe)")

if __name__ == "__main__":
    conf = configparser.RawConfigParser()
    if not path.exists(conf_file):
        open(conf_file, 'w')
    conf.read(conf_file)
    Table()
    main()
